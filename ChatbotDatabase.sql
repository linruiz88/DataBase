/********************
	建立資料庫
********************/
create database $(DBName) on
(
	NAME = $(DBName)_data,
	FILENAME = '$(DirPath)\$(DBName)_data.mdf',
	SIZE = 10,
	FILEGROWTH = 5
)
log on
(
	NAME = $(DBName)_log,
	FILENAME = '$(DirPath)\$(DBName)_log.ldf',
	SIZE = 10,
	FILEGROWTH = 5
)
go
--sp_dboption '$(DBName)', 'select into/bulkcopy', TRUE /*MS Sql Server從2008後就預計移除這個功能，請避免使用
go

-- 加入資料庫版本設定  SQL Server 2005(90)、2008(100)、2012(110)、2014(120)、2016(130)
alter database [$(DBName)] set compatibility_level = $(DBVersion)
go
-- 設定資料庫定序
alter database [$(DBName)] set single_user with rollback immediate
alter database [$(DBName)] collate $(DBCollate)
alter database [$(DBName)] set multi_user with rollback immediate
go

/********************
	建立預設的Tables
********************/
use [$(DBName)]
go

/****************
	Basic Schema
	tables: Entity, Object, ORel
****************/

-- Entity Table
create table Entity
(
	EID smallint identity(1,1) not null,
	CName nvarchar(50) not null,
	EName nvarchar(50) not null,
	bORel bit not null default(1),

	constraint PK_Entity primary key clustered (EID),
	constraint UQ_Entity_CName unique nonclustered (CName asc),
	constraint UQ_Entity_EName unique nonclustered (EName asc)
)
go

-- Object Table
create table Object
(
	OID int identity(1,1) not null,
	Type smallint not null,
	CName nvarchar(255) null,
	CDes nvarchar(800) null,
	EName nvarchar(255) null,
	EDes nvarchar(800) null,
	Since datetime not null default(getdate()),
	LastModifiedDT datetime not null default(getdate()),
	OtherDT datetime not null default(getdate()),
	DataByte binary(1) not null default(0),	-- Process bit(i)， 1: N/A, 2: Sentence TP, 3: TP, 4: DOM Parser, 5: Crawler, 6: N/A, 7: N/A, 8: N/A
	OwnerMID int null,				-- FK: Member.MID，物件的擁有者
	nClick int not null default(0),	-- 物件被點閱次數
	nInlinks int null default(0),	-- total citated counts (inlinks)
	nOutlinks int null default(0),	-- total citations (outlinks)
	bHided bit not null default(0),	-- 0: display 1: hide

	constraint PK_Object primary key clustered (OID),
	constraint FK_Object_Type foreign key (Type) references Entity(EID)
)
create nonclustered index IX_Object_CName on Object(CName asc)
go
create nonclustered index IX_Object_EName on Object(EName asc)
go

-- ORel Table
create table ORel
(
	OID1 int not null,
	OID2 int not null,
	Rank int null,
	Des nvarchar(255) null,

	constraint PK_ORel primary key clustered (OID1, OID2),
	constraint FK_ORel_OID1 foreign key (OID1) references Object(OID),
	constraint FK_ORel_OID2 foreign key (OID2) references Object(OID)
)
go

/****************
	Concept Hierarchy Schema
	tables: CLayout, Class, Inheritance, CO
****************/

-- Layout (main frame for page)
-- create table CLayout
-- (
-- 	LID int not null identity(1, 1),
-- 	LName nvarchar(255),
-- 	LDes nvarchar(900),
-- 	constraint PK_CLayout primary key clustered (LID)
-- )

---- Block (javascript template)
--create table CBlock
--(
--	BID int not null identity(1, 1),
--	BName nvarchar(255),
--	constraint PK_CBlock primary key clustered (BID)
--)

---- Data(view by naming policy vd_*)
--create table CData
--(
--	DID int not null identity(1, 1),
--	DName varchar(255),
--	NSpace varchar(20),
--	constraint PK_CData primary key clustered (DID)
--)

-- Class Table
-- Class Definition: Directory, Category
create table Class
(
	CID int identity(1,1) not null,
	Type smallint null,
	CName nvarchar(255) null default(''),
	CDes nvarchar(800) null default(''),
	EName nvarchar(255) null default(''),
	EDes nvarchar(800) null default(''),
	IDPath nvarchar(255) null,
	NamePath nvarchar(850) null,
	Since datetime not null default(getdate()),
	LastModifiedDT datetime not null default(getdate()),
	nObject int not null default(0),			-- 目錄底下的物件個數
	cRank tinyint null default(0),				-- Sorting policy for subdirectories (i.e., Inheritance)
	oRank tinyint null default(0),				-- Sorting policy for Objects (i.e., CO)
	nLevel tinyint null,
	ImgID smallint not null default(0),
	nClick int not null default(0),				-- 目錄被點選次數
	Keywords nvarchar(255) null default(''),	-- separated by ","
	bHided bit not null default(0),				-- 0: display 1: hide

 	constraint PK_Class primary key clustered (CID),
	constraint UQ_Class_IDPath unique nonclustered (IDPath),
	constraint UQ_Class_NamePath unique nonclustered (NamePath),
	constraint FK_Class_Type foreign key (Type) references Entity(EID)
)
go
create nonclustered index IX_Class_CName on Class(CName asc)
go
create nonclustered index IX_Class_EName on Class(EName asc)
go

-- Inheritance Table
-- Inheritance (Relationship): Direct subclasses of Class (Class-Class)
-- * In default, inheritance is a tree strucuture
create table Inheritance
(
	PCID int not null,
	CCID int not null,
	Rank smallint null,
	MG tinyint null,

	constraint PK_Inheritance primary key clustered (PCID, CCID),
	constraint FK_Inheritance_PCID foreign key (PCID) references Class(CID),
	constraint FK_Inheritance_CCID foreign key (CCID) references Class(CID)
)
go

-- CO Table
-- CO (Relationship): Direct child Objects of the Class (Class-Object)
create table CO
(
	CID int not null,
	OID int not null,
	Rank smallint null,		-- Object ranks in the Class
	MG tinyint null,		-- Membership Grade for Automatic Classification
	Since datetime not null default(getdate()),

	constraint PK_CO primary key clustered (CID, OID),
	constraint FK_CO_CID foreign key (CID) references Class(CID),
	constraint FK_CO_OID foreign key (OID) references Object(OID)
)
go

/****************
	URI Schema
	tables: StatusCode, URLScheme, ContentType, Language, URL
****************/

-- StatusCode table
create table StatusCode
(
	Status int not null,
	Msg nvarchar(64) null,
	CDes nvarchar(800) null,

	constraint PK_Status primary key clustered (Status)
)
go

-- Url Scheme table , e.g. HTTP, HTTPS, FTP
create table URLScheme
(
	SID smallint identity(1,1) not null,
	[Scheme] nvarchar(10) not null,
	CDes nvarchar(255) null,

	constraint PK_URLScheme primary key clustered (SID),
	constraint UQ_URLScheme unique nonclustered ([Scheme])
)
go

-- ContentType table
create table ContentType
(
	CTID smallint identity(1,1) not null,
	Title nvarchar(255) not null,
	Des nvarchar(255) null,		-- Unicode (UTF-8) for English or Chinese

	constraint PK_ContentType primary key clustered (CTID),
	constraint UQ_ContentType unique nonclustered (Title asc)
)
go

-- LID is set on Language (目錄) for URL
create table [Language]
(
	LID smallint identity(1,1) not null,
	CName nvarchar(50) null,
	EName nvarchar(50) null,
	LName nvarchar(255) null,
	Code nvarchar(20) null,

	constraint PK_Language primary key clustered (LID)
)
go
create nonclustered index IX_Language_CName on [Language](CName asc)
go
create nonclustered index IX_Language_EName on [Language](EName asc)
go
create nonclustered index IX_Language_Code on [Language](Code asc)
go

-- URL table
create table URL
(
	UID int not null,
	Scheme smallint not null,
	HostName varchar(900) not null,
	Path nvarchar(900) not null default('/'),
	Title nvarchar(255) null,		-- <title> ... </title>
	Des nvarchar(1024) null,
	Lang smallint null,				-- FK.Language.LID
	ContentLen int null,
	Keywords nvarchar(255) null,
	Indexable bit null,				-- 是否適合索引搜尋
	IndexInfo nvarchar(255) null,	-- Only index Content Blocks, how to define CB?
	SID int null,					-- FK: StatusCode.Status
	MD5URL binary(16) not null,
	MD5 binary(16) null,
	ContentType smallint null default(1),	-- FK.ContentType.CTID
	Weight tinyint null,			-- the quality of an URL document
	Crawl tinyint null,				-- crawl depth
	ModifiedFreq int null,
	OKFreq int null,
	--QueryGet nvarchar(900) null,
	--QueryPost nvarchar(4000) null,
	--JID int null,
	constraint PK_URL primary key clustered (UID),
	constraint FK_URL_UID foreign key (UID) references Object(OID),
	constraint FK_URL_Scheme foreign key (Scheme) references URLScheme(SID),
	constraint FK_URL_SID foreign key (SID) references StatusCode(Status),
	constraint FK_URL_Lang foreign key (Lang) references [Language](LID),
	constraint FK_URL_ContentType foreign key (ContentType) references ContentType(CTID),
	constraint UQ_URL_MD5URL unique nonclustered (MD5URL),
	constraint UQ_URL_MD5 unique nonclustered (MD5)
)
go

/****************
	User Schema
	tables: Member, Groups, GM, Permission, Nation
****************/

-- Member: Extension table from Object, Detail for member (user) information
create table Member
(
	MID int not null,
	Account nvarchar(50) not null,
	PWD nvarchar(40) null,
	Valid bit null,						-- 1: pass, 0: freeze, null: non-active
	LastLoginDT datetime null,
	LoginErrCount tinyint not null default(0),	-- 0: 登入成功, else: 若該值達錯誤上限，Valid = 0，禁止使用者登入
	EMail nvarchar(100) null,
	Status tinyint null,				-- null: 等待系統管理者審核, 0: 等待使用者確認, 1: 正常
	Sex bit null,						-- null: 尚未設定, 1: 男生, 0:女生
	LoginCount int not null default(0),
	VerifyCode nvarchar(50) null,		-- for registration
	CID int null,						-- Personal directory
	Birthday smalldatetime null,
	Nation tinyint null,
	Address nvarchar(200) null,
	Phone nvarchar(25) null,
	SendEMailOK bit null,

	constraint PK_Member primary key clustered (MID),
	constraint UQ_Member_Account unique nonclustered ([Account] asc),
	constraint FK_Member_MID foreign key (MID) references Object(OID)
)
go
alter table Object with check add constraint FK_Object_OwnerMID foreign key (OwnerMID) references Member(MID)
go

-- Groups table
create table Groups
(
	GID int identity(1,1) not null,
	Type tinyint null,				-- 0: 系統群組, 1: 使用者自建群組
	GName nvarchar(40) not null,
	GDes nvarchar(1024) null,
	Status tinyint null,			-- 0: 等待系統管理者審核, 1: 正常
	Since datetime not null default(getdate()),

	constraint PK_Groups primary key clustered (GID),
	constraint UQ_Groups unique nonclustered (GName asc)
)
go
--exec sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0為等待系統管理者審核，1為正常' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Groups', @level2type=N'COLUMN', @level2name=N'Status'
--GO
--exec sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0為系統群組，1為使用者自建群組' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Groups', @level2type=N'COLUMN', @level2name=N'Type'
--go

-- GroupMember table
create table GM
(
	GID int not null,
	MID int not null,
	Role tinyint not null,  -- 0: Owner, 1: Manager, 2: Member, ...
	Type bit null,			-- 0: 邀請, 1: 申請, null: 直接加入
	Status bit null,		-- 0: 申請中, 1: 正常, null: 過期使用者

	constraint PK_GownUs primary key clustered (GID, MID),
	constraint FK_GM_GID foreign key (GID) references Groups(GID),
	constraint FK_GM_MID foreign key (MID) references Member(MID)
)
go

-- Permission is set on Class (目錄) for Groups or Member
create table Permission
(
	CID int not null,		-- Permission主要設定在Class，但也可以是Object (OID)
	RoleType bit not null,	-- 0: Class, 1: Object
	RoleID int not null,	-- CID or OID
	PermissionBits tinyint not null default(1), -- default View， bit(i) ， 1: V, 2: R, 3: I, 4: U, 5: D, 6: M, 7: S

	constraint UQ_Permission unique nonclustered (CID, RoleType, RoleID)
)
go

-- NID is set on Nation (目錄) for Member
create table Nation
(
	NID tinyint identity(1,1) not null,
	CountryCode char(2),	-- Country abbreviation
	CName nvarchar(255) null,
	EName nvarchar(255) null,

	constraint PK_Nation primary key clustered (NID)
)
go
create nonclustered index IX_Nation_CName on Nation(CName asc)
go
create nonclustered index IX_Nation_EName on Nation(EName asc)
go
alter table Member with check add constraint FK_Member_Nation foreign key (Nation) references Nation(NID)
go

/****************
	Log Diary Schema
	tables: UserAgent, MSession, Log, LogDir, LogObject LogMan, LogManTx, LogSearch, LogError
****************/

create table UserAgent
(
	UAID int identity(1,1) not null,
	UAString varchar(900) not null,
	Since datetime not null default(getdate()),

	constraint PK_UserAgent primary key clustered (UAID),
	constraint UK_UserAgent_UAString unique nonclustered (UAString)
)
go

-- Member Session (MSession): create new session for each connection
-- Log以session為單位：PassportCode in Cookie, map to primary key SID
-- MSession to Log* is: 1-to-N
create table MSession
(
	SID int identity(1,1) not null,
	MID int not null default(0),		-- Guest = 0。不需FK to Object.OID or Member.MID
	IP nvarchar(16) not null,
	UserAgent int null,
	PassportCode nvarchar(32) not null,	-- UQ = MD5(MID, IP)
	Since datetime not null default(getdate()),
	LastModifiedDT datetime not null default(getdate()),
	ExpiredDT datetime null,

	constraint PK_MSession primary key clustered (SID),
	constraint FK_MSession_UA foreign key (UserAgent) references UserAgent(UAID)
)
go

-- Log table
create table [Log]
(
	LID bigint identity(1,1) not null,	--MSession.SID
	SID int not null,	-- MSession.SID
	Type tinyint null,	-- 1: Class, 2: Object, 3: Man, 4: ManTx, 5: Err, Others ...
	Since datetime not null default(getdate()),

	constraint PK_Log primary key clustered (LID),
	constraint FK_Log_SID foreign key (SID) references MSession(SID)
)
go

-- Browse Directories ==> Index.aspx ==> Class (Directories)
create table LogDir
(
	LID bigint not null,
	CID int not null,		-- 不需FK，因為使用者可能亂產生CID來測試：不存在==>Fail
	Operation bit not null,	-- Operation: success or fail
	Sort tinyint null,		-- Use which sorting method

	constraint PK_LogDir primary key clustered (LID),
	constraint FK_LogDir_LID foreign key (LID) references [Log](LID)
)
go

-- Show Objects ==> ShowObject.aspx
create table LogObject
(
	LID bigint not null,
	OID int not null,
	Operation bit not null, -- Operation: success or fail
	IndexCount int null,

	constraint PK_LogObject primary key clustered (LID),
	constraint FK_LogObject_LID foreign key (LID) references [Log](LID)
)
go

-- Browse with Permission ==> Manage.aspx
create table LogMan
(
	LID bigint not null,
	TargetType bit null,	-- Class: 0, Object: 1
	TargetID int null,		-- CID or OID
	Operation bit not null,	-- Operation: success or fail
	Sort tinyint null,		--
	IndexCount int null,	-- Page number or Object ranking #

	constraint PK_LogMan primary key clustered (LID),
	constraint FK_LogMan_LID foreign key (LID) references [Log](LID)
)
go

-- All transactions ==> Tx<InserClass>.aspx, ...
create table LogManTx
(
	LID bigint not null,
	Method bit null,				-- Insert/delete/update = null/0/1
	PostString nvarchar(4000) null,	-- 可以記錄，就記錄全部，不行的話，紀錄 <max-20> + MD5(剩下的部分)

	constraint PK_LogManTx primary key clustered (LID),
	constraint FK_LogManTx_LID foreign key (LID) references [Log](LID)
)
go

create table LogSearch
(
	LID bigint not null,
	QueryString nvarchar(60) not null,

	constraint FK_LogSearch primary key clustered (LID),
	constraint FK_LogSearch_LID foreign key (LID) references [Log](LID)
)
go

create table LogError
(
	LID bigint not null,
	ErrCode int null,
	ErrMsg nvarchar(900),

	constraint PK_LogError primary key clustered (LID),
	constraint FK_LogError_LID foreign key (LID) references [Log](LID)
)

/****************
	Other Extension Table Schema
****************/

-- Archive Table
create table Archive
(
	AID int not null,
	[FileName] nvarchar(128) not null,
	Keywords nvarchar(255) not null default(''),	-- separated by ","
	Lang smallint null,
	Indexable bit null,
	IndexInfo nvarchar(255) null default(''),
	ContentLen int null,
	MD5 binary (16) null,
	ContentType smallint null default(0),

	constraint PK_Archive primary key clustered (AID),
	constraint FK_Archive_AID foreign key (AID) references Object(OID),
	constraint FK_Archive_CTID foreign key (ContentType) references ContentType(CTID)
)
go
create index IX_Archive_MD5 on Archive(MD5)
go
create index IX_Archive_ContentLen on Archive(ContentLen)
go

-- Post Table
create Table Post
(
	PID int not null,
	Detail nvarchar(max),

	constraint PK_Post primary key clustered (PID),
	constraint FK_Post_PID foreign key (PID) references Object(OID)
)
go

-- DCField Table
create table DCField
(
	DCID smallint identity(1,1) not null,
	CName nvarchar(20) null,
	EName nvarchar(20) not null,

	constraint PK_DCField primary key clustered (DCID),
	constraint UQ_DCField_EName unique nonclustered (EName asc)
)
go
create nonclustered index IX_DCField_CName on DCField(CName asc)
go

-- EntityM2DC Table
create table EntityM2DC
(
	EID smallint not null,
	Field nvarchar(30) not null,
	DCField smallint not null,
	SNo tinyint default(0) not null,
	JsonField nvarchar(20) not null,
	Caption nvarchar(20) null,

	constraint PK_EntityM2DC primary key clustered (EID, DCField, SNo, JsonField),
	constraint FK_EntityM2DC_EID foreign key (EID) references Entity(EID),
	constraint FK_EntityM2DC_DCField foreign key (DCField) references DCField(DCID)
)
go
create nonclustered index IX_EntityM2DC_JsonField on EntityM2DC(JsonField asc)
go
